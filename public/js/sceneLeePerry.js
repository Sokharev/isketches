import { OBJLoader } from "../third_party/OBJLoader.js";
import { Matrix4, Group, Mesh } from "../third_party/three.module.js";

let leePerry;
const group = new Group();
let material;

const params = {};

async function loadModel(file) {
  return new Promise((resolve, reject) => {
    const loader = new OBJLoader();
    loader.load(file, resolve, null, reject);
  });
}

async function loadLeePerry() {
  //const model = await loadModel("../assets/LeePerry.obj");
  const model = await loadModel("../assets/uploads_files_2800448_low_poly_interior_houses.obj");
  const geo = model.children[3].geometry;
  geo.center();
  const scale = 10;
  geo.applyMatrix4(new Matrix4().makeScale(scale, scale, scale));
  return geo;
}

async function generate() {
  if (leePerry) {
    group.remove(leePerry);
  }
  const geo = await loadLeePerry();
  geo.center();
  const scale = 0.5;
  geo.applyMatrix4(new Matrix4().makeScale(scale, scale, scale));
  // geo.computeVertexNormals();
  // geo.computeFaceNormals();
  leePerry = new Mesh(geo, material);
  leePerry.castShadow = leePerry.receiveShadow = false;
  group.add(leePerry);
}

const obj = {
  init: async (m, q, r) => {
    material = m;
    await generate();
  },
  update: () => {},
  group,
  generate: () => generate(material),
  params: (gui) => {},
};

export { obj };
